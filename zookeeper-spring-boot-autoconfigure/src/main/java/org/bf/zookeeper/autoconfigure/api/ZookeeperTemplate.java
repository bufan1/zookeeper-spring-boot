package org.bf.zookeeper.autoconfigure.api;

import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.zookeeper.CreateMode;

import java.util.concurrent.TimeUnit;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
public interface ZookeeperTemplate {

    /**
     * 新增节点
     *
     * @param path       path
     * @param content    content
     * @param createMode createMode
     */
    void createNode(String path, String content, CreateMode createMode) throws Exception;

    /**
     * 删除节点
     *
     * @param path nodepath
     */
    void deleteNode(String path) throws Exception;

    /**
     * 删除节点
     *
     * @param path    path
     * @param version version
     */
    void deleteNode(String path, int version) throws Exception;

    /**
     * 获取数据
     *
     * @param path path
     * @return byte
     * @throws Exception
     */
    byte[] get(String path) throws Exception;


    /**
     * 获取锁
     * @param path
     * @return
     * @throws Exception
     */
    boolean lock(String path) throws Exception;

    /**
     * 获取锁
     * @param path
     * @param time
     * @param timeUnit
     * @return
     * @throws Exception
     */
    boolean lock(String path, Long time, TimeUnit timeUnit) throws Exception;

    /**
     * 释放锁
     * @param path
     * @return
     * @throws Exception
     */
    boolean release(String path) throws Exception;


    /**
     * 监听节点
     * @param path
     * @param nodeCacheListener
     * @throws Exception
     */
    void watchNode(String path, NodeCacheListener nodeCacheListener) throws Exception;

    /**
     * 监听节点下的子节点
     * @param path
     * @param pathChildrenCacheListener
     * @throws Exception
     */
    void watchNodeChild(String path, PathChildrenCacheListener pathChildrenCacheListener) throws Exception;


    /**
     * 监听树
     * @param path
     * @param treeCacheListener
     * @throws Exception
     */
    void watchNodeTree(String path, TreeCacheListener treeCacheListener) throws Exception;

    /**
     * 设置数据
     *
     * @param path  path
     * @param bytes bytes
     * @throws Exception
     */
    void set(String path, byte[] bytes) throws Exception;
}
