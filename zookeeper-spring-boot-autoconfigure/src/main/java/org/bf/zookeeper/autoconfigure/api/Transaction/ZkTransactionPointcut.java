package org.bf.zookeeper.autoconfigure.api.Transaction;

import org.bf.zookeeper.autoconfigure.ZookeeperTrans;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;

/**
 * Created on 2021/7/9.
 *
 * @author BUFAN
 */
public class ZkTransactionPointcut extends StaticMethodMatcherPointcut {



    @Override
    public boolean matches(Method method, Class<?> aClass) {
        ZookeeperTrans zookeeperTrans = AnnotationUtils.findAnnotation(method, ZookeeperTrans.class);
        return zookeeperTrans != null;
    }


}
