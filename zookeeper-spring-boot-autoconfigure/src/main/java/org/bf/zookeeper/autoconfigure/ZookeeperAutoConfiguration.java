package org.bf.zookeeper.autoconfigure;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.bf.zookeeper.autoconfigure.api.Transaction.TransInterceptor;
import org.bf.zookeeper.autoconfigure.api.Transaction.TransactionManger;
import org.bf.zookeeper.autoconfigure.api.Transaction.ZkTransactionPointcut;
import org.bf.zookeeper.autoconfigure.api.ZookeeperBaseTemplate;
import org.bf.zookeeper.autoconfigure.api.ZookeeperTemplate;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
@EnableConfigurationProperties(ZookeeperProperties.class)
@Configuration
@ComponentScan(value = "org.bf.zookeeper.autoconfigure.api")
public class ZookeeperAutoConfiguration {

    @Autowired
    ZookeeperProperties zookeeperProperties;

    @Bean
    public CuratorFramework createCuratorFramework() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(zookeeperProperties.getBaseSleepTimeMs(), zookeeperProperties.getMaxRetryCount());
        CuratorFramework client = CuratorFrameworkFactory.builder().connectString(zookeeperProperties.getServers())
                .connectionTimeoutMs(zookeeperProperties.getConnectionTimeout())
                .sessionTimeoutMs(zookeeperProperties.getSessionTimeout())
                .retryPolicy(retryPolicy)
                .build();
        client.start();
        return client;
    }

    @Bean
    public TransactionManger transactionManger(CuratorFramework curatorFramework) {
        TransactionManger transactionManger = new TransactionManger();
        transactionManger.setCuratorFramework(curatorFramework);
        return transactionManger;
    }

    @Bean
    public DefaultPointcutAdvisor defaultPointcutAdvisorZk(TransactionManger transactionManger) {
        TransInterceptor transInterceptor = new TransInterceptor();
        transInterceptor.setTransactionManger(transactionManger);
        ZkTransactionPointcut pointcut = new ZkTransactionPointcut();
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
        advisor.setPointcut(pointcut);
        advisor.setAdvice(transInterceptor);
        return advisor;
    }

    @Bean
    public ZookeeperTemplate createZookeeperTemplate(CuratorFramework curatorFramework) {
        return new ZookeeperBaseTemplate(curatorFramework);
    }


}
