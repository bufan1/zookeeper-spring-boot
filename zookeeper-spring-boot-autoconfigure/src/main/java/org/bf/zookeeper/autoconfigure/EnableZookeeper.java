package org.bf.zookeeper.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ZookeeperAutoConfiguration.class)
public @interface EnableZookeeper {
}
