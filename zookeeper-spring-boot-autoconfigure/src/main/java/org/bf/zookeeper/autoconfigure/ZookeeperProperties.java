package org.bf.zookeeper.autoconfigure;


import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
@ConfigurationProperties(prefix = "zookeeper")
public class ZookeeperProperties {

    /**
     * 服务地址
     */
    private String servers;

    /**
     * 会话超时时间
     */
    private int sessionTimeout = 30*1000;


    /**
     * 连接超时时间
     */
    private int connectionTimeout = 3*1000;

    /**
     * 初次重试时间
     */
    private int baseSleepTimeMs = 3*1000;

    /**
     * 最大重试次数
     */
    private int maxRetryCount = 5;


    public String getServers() {
        return servers;
    }

    public void setServers(String servers) {
        this.servers = servers;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getBaseSleepTimeMs() {
        return baseSleepTimeMs;
    }

    public void setBaseSleepTimeMs(int baseSleepTimeMs) {
        this.baseSleepTimeMs = baseSleepTimeMs;
    }

    public int getMaxRetryCount() {
        return maxRetryCount;
    }

    public void setMaxRetryCount(int maxRetryCount) {
        this.maxRetryCount = maxRetryCount;
    }
}
