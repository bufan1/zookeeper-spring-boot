package org.bf.zookeeper.autoconfigure.api.Transaction;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
public interface Transaction {
    /**
     * 获取事务
     */
    void getTransaction();

    /**
     * 提交
     */
    void commit();

    /**
     * 回退
     */
    void rollBack();
}
