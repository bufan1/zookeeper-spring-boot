package org.bf.zookeeper.autoconfigure.api;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.transaction.CuratorTransaction;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.zookeeper.CreateMode;
import org.bf.zookeeper.autoconfigure.api.Transaction.TransactionManger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
public class ZookeeperBaseTemplate implements ZookeeperTemplate {

    private final ThreadLocal<Map<String, InterProcessMutex>> localLockMap = new ThreadLocal<>();

    CuratorFramework curatorFramework;

    public ZookeeperBaseTemplate(CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }

    @Override
    public void createNode(String path, String content, CreateMode createMode) throws Exception {
        if (getTrans() != null) {
            updateTrans(getTrans().create().withMode(createMode).forPath(path, content == null ? null : content.getBytes()).and());
        } else {
            curatorFramework
                    .create()
                    .creatingParentContainersIfNeeded()
                    .withMode(createMode)
                    .forPath(path, content == null ? null : content.getBytes());
        }

    }

    @Override
    public void deleteNode(String path) throws Exception {
        if (getTrans() != null) {
            updateTrans(getTrans().delete().forPath(path).and());
        } else {
            curatorFramework.delete().guaranteed().deletingChildrenIfNeeded().forPath(path);
        }


    }

    @Override
    public void deleteNode(String path, int version) throws Exception {
        if (getTrans() != null) {
            updateTrans(getTrans().delete().withVersion(version).forPath(path).and());
        } else {
            curatorFramework.delete().guaranteed().deletingChildrenIfNeeded().withVersion(version).forPath(path);
        }

    }

    @Override
    public byte[] get(String path) throws Exception {
        return curatorFramework.getData().forPath(path);
    }

    @Override
    public boolean lock(String path) throws Exception {
        return lock(path, -1L, (TimeUnit) null);
    }

    @Override
    public boolean lock(String path, Long time, TimeUnit timeUnit) throws Exception {
        if (localLockMap.get() == null) {
            localLockMap.set(new HashMap<>());
        }
        InterProcessMutex lock = null;
        if (localLockMap.get().get(path) == null) {
            lock = new InterProcessMutex(curatorFramework, path);
        } else {
            lock = localLockMap.get().get(path);
        }
        boolean result = lock.acquire(time, timeUnit);
        if (result) {
            localLockMap.get().put(path, lock);
        }
        return result;
    }

    @Override
    public boolean release(String path) throws Exception {
        if (localLockMap.get().get(path) != null) {
            localLockMap.get().get(path).release();
            localLockMap.get().remove(path);
            return true;
        }
        return false;
    }

    @Override
    public void watchNode(String path, NodeCacheListener nodeCacheListener) throws Exception {
        NodeCache nodeCache = new NodeCache(curatorFramework, path);
        nodeCache.getListenable().addListener(nodeCacheListener);
        nodeCache.start(true);
    }

    @Override
    public void watchNodeChild(String path, PathChildrenCacheListener pathChildrenCacheListener) throws Exception {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(curatorFramework, path, true);
        pathChildrenCache.getListenable().addListener(pathChildrenCacheListener);
        //属性表示是否初始化的时候读取zookeeper配置，防止第一次通知
        pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
    }

    @Override
    public void watchNodeTree(String path, TreeCacheListener treeCacheListener) throws Exception {
        TreeCache treeCache = new TreeCache(curatorFramework, path);
        treeCache.getListenable().addListener(treeCacheListener);
        treeCache.start();
    }


    @Override
    public void set(String path, byte[] bytes) throws Exception {
        if (getTrans() != null) {
            updateTrans(getTrans().setData().forPath(path, bytes).and());
        } else {
            curatorFramework.setData().forPath(path, bytes);
        }

    }

    public CuratorTransaction getTrans() {
        if (TransactionManger.existTrans()) {
            return TransactionManger.getTrans();
        } else {
            return null;
        }
    }

    public void updateTrans(CuratorTransaction curatorTransaction) {
        TransactionManger.update(curatorTransaction);
    }

}
