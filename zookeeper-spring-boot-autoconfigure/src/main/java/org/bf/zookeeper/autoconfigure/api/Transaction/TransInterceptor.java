package org.bf.zookeeper.autoconfigure.api.Transaction;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.bf.zookeeper.autoconfigure.ZookeeperTrans;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created on 2021/7/9.
 *
 * @author BUFAN
 */
public class TransInterceptor implements MethodInterceptor {

    private TransactionManger transactionManger;

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        transactionManger.openTransaction();
        try {
            Object proceed = methodInvocation.proceed();
            transactionManger.commit();
            return proceed;
        } catch (Exception e) {
            throw e;
        } finally {
            transactionManger.clearTransaction();
        }


    }

    public TransactionManger getTransactionManger() {
        return transactionManger;
    }

    public void setTransactionManger(TransactionManger transactionManger) {
        this.transactionManger = transactionManger;
    }
}
