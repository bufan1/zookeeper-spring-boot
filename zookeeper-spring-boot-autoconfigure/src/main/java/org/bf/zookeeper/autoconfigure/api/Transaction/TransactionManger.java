package org.bf.zookeeper.autoconfigure.api.Transaction;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.transaction.CuratorTransaction;
import org.apache.curator.framework.api.transaction.CuratorTransactionBridge;
import org.apache.curator.framework.api.transaction.CuratorTransactionFinal;
import org.springframework.core.NamedThreadLocal;

import java.util.Map;

/**
 * Created on 2021/7/9.
 *
 * @author BUFAN
 */
public class TransactionManger {

    private CuratorFramework curatorFramework;

    private static final ThreadLocal<CuratorTransaction> CURATOR_TRANSACTION_THREAD_LOCAL = new NamedThreadLocal<>("Transactional resources");



    public CuratorFramework getCuratorFramework() {
        return curatorFramework;
    }

    public void setCuratorFramework(CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }


    public void openTransaction() {
        if (CURATOR_TRANSACTION_THREAD_LOCAL.get() == null) {
            CURATOR_TRANSACTION_THREAD_LOCAL.set(curatorFramework.inTransaction());
        }
    }

    public void clearTransaction() {
        CURATOR_TRANSACTION_THREAD_LOCAL.remove();
    }


    public void commit() throws Exception {
        if (CURATOR_TRANSACTION_THREAD_LOCAL.get() != null && CURATOR_TRANSACTION_THREAD_LOCAL.get() instanceof CuratorTransactionFinal) {
            ((CuratorTransactionFinal) CURATOR_TRANSACTION_THREAD_LOCAL.get()).commit();
        }
    }

    public static boolean existTrans() {
        return CURATOR_TRANSACTION_THREAD_LOCAL.get() != null;
    }


    public static CuratorTransaction getTrans() {
        return CURATOR_TRANSACTION_THREAD_LOCAL.get();
    }

    public static void update(CuratorTransaction curatorTransaction) {
       CURATOR_TRANSACTION_THREAD_LOCAL.set(curatorTransaction);
    }

}
