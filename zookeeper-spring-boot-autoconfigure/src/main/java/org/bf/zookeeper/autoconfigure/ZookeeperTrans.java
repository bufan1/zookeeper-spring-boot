package org.bf.zookeeper.autoconfigure;

import java.lang.annotation.*;

/**
 * Created on 2021/7/7.
 *
 * @author BUFAN
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ZookeeperTrans {
}
